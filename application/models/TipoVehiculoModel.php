<?php
class TipoVehiculoModel extends CI_Model{
    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }

    function SaveTipoVehiculo($id,$Descripcion,$estado)
    {
        $status = $estado == "on" ? 1 : 2;
        
        if ($id > 0) {
            $array = array(
                'Descripcion' => $Descripcion,
                'Estado' => $status
        );
            $this->db->set($array);
            $this->db->where('Id', $id);
            $query =  $this->db->update('tipos_vehiculos');
            return $query;
        }else{
            $data = array(
                'Descripcion' => $Descripcion,
                'Estado' => $status
                );
            $query =   $this->db->insert('tipos_vehiculos', $data);
            return $query;
        }

    }

    public function getAllTipoVehivulos($all)
    {
        if ($all == 1) {
            $this->db->select('Id,Descripcion, Estado');
            $this->db->from('tipos_vehiculos');
            $this->db->where('Estado', 1);
            $query = $this->db->get(); 
            return $query->result();
        }else{
            $this->db->select('Id,Descripcion, Estado');
            $this->db->from('tipos_vehiculos');
            $query = $this->db->get(); 
            return $query->result();
        }

    }

    public function getTipoVehiculosById($id)
    {
                 $this->db->select('Id,Descripcion, Estado');
                 $this->db->from('tipos_vehiculos');
                 $this->db->where('Id', $id);
                $query = $this->db->get(); 
                return $query->result();
    }
    public function UpdateTipoVehiculo($id)
    {

        try{
            $this->db->set('Estado', 2,false);
            $this->db->where('Id', $id);
            $query =  $this->db->update('tipos_vehiculos');
            return $query;
           
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

    }
}

