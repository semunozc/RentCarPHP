<?php

class UserModel extends CI_Model{

    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }
    function getAll(){
     $query = $this->db->query("SELECT COUNT(*) ClienteCantidad  FROM clientes;");
     return $query->result();
    }
    function getUserData($user,$pass)
    {
        //$query = $this->db->get_where('usuarios', array('Usuario' => $user,'Contrasenia' => $pass,'Role' => 1));
        $this->db->select('Id_Usuario,Usuario,Role,Nombre,Apellido');
        $this->db->from('usuarios');
        $this->db->join('empleados', 'empleados.Id_Empleado = usuarios.Id_Usuario');
        $this->db->where('Usuario', $user);
        $this->db->where('Contrasenia', $pass);
        $this->db->where('Role', 1);
        $query = $this->db->get();
        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }     
            return $query->result();
    }
   }

