<?php
class RentModel extends CI_Model{
    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }

    function SaveVehiculoRentado($id_empleado,$id_vehiculo,$id_Cliente,$Fecha_devolucion,$MontoXDia2,$CantidadDias2,$Comentarios2)
    {
        $array = array(
            'id_Empleado' => $id_empleado,
            'Id_Vehiculo' => $id_vehiculo,
            'Id_cliente' => $id_Cliente,
            'Fecha_Renta' => date("Y/m/d"),
            'Fecha_Devolución' => $Fecha_devolucion,
            'MontoXDía' => $MontoXDia2,
            'Cantidad_días' => $CantidadDias2,
            'Comentario' => $Comentarios2,
            'Estado' => 1
              );
            $query =   $this->db->insert('rentaydevolucion', $array);
            return $query;
    }
    function SaveInspeccionVehiculo($id_vehiculo,$id_Cliente,$ralladura,$combstible,$Gomarepuesto,$Gato,$cristal,$GomaEstado,$id_empleado)
    {
        $array = array(
            'Vehiculo' => $id_vehiculo,
            'Id_Cliente' => $id_Cliente,
            'Tiene_Ralladuras' => $ralladura,
            'Cantidad_Conbustible' => $combstible,
            'TieneGomaRepuesto' => $Gomarepuesto,
            'TieneGato' => $Gato,
            'TieneRoturaCrsital' => $cristal,
            'EstadoGomas' => json_encode($GomaEstado),
            'Fecha' => date("Y/m/d"),
            'Empleado_inspeccion' => $id_empleado,
            'Estado' => 1
              );
            $query =   $this->db->insert('inspeccion', $array);
            return $query;
    }
    public function getAllVehiculos($all)
    {
        $query =  $this->db->query('SELECT e.Descripcion DescripEstados,v.Id_Vehiculo,v.Descripcion,v.No_Chasis,v.No_Motor,v.No_Placa,e.Id_Estado estado ,md.Descripcion modelo,m.`Descripcion` marca,t.Descripcion combustible,tv.Descripcion tipovehiculo FROM vehiculos v 
        JOIN estados e ON (e.Id_Estado = v.Estado)
        JOIN marcas m ON (m.Id_Marca = v.Marca)
        JOIN modelos md ON (md.Id_Modelo = v.Modelo)
        JOIN tipos_combustible t ON (t.Id_Combustible = v.Tipo_Combustible)
        JOIN tipos_vehiculos tv ON (tv.Id = v.Tipo_Vehiculo)
        
        WHERE  e.Id_Estado = '. $all);
        return $query->result();
    }
//     public function getAllMarcas($all)
//     {
 
//             $this->db->select('Id_Marca,Descripcion, Estado');
//             $this->db->from('marcas');
//             $query = $this->db->get(); 
//             return $query->result();

//     }
//     public function getAllTipoConbustible()
//     {
//             $this->db->select('Id_Combustible,Descripcion, Estado');
//             $this->db->from('tipos_combustible');
//             $this->db->where('Estado', 1);
//             $query = $this->db->get(); 
//             return $query->result();
//     }
//     public function getAllModelo($id)
//     {
//         if ($id > 0) {
//             $query =  $this->db->query('SELECT m.Id_Modelo,mc.Descripcion Marca,m.Descripcion Modelo,m.Id_Modelo, e.Descripcion Estado FROM Modelos m JOIN estados e ON (m.Estado = e.Id_Estado)
//             JOIN Marcas mc ON (mc.Id_Marca = m.Id_Marca) AND m.Id_Marca ='.$id);
//             return $query->result();
//         }
//    }
//    public function getAllTipoVehiculos()
//    {
//            $this->db->select('Id,Descripcion, Estado');
//            $this->db->from('tipos_vehiculos');
//            $this->db->where('Estado', 1);
//            $query = $this->db->get(); 
//            return $query->result();
//    }
    // public function getModeloById($id)
    // {
    //              $this->db->select('Id_Modelo,Id_Marca, Descripcion,Estado');
    //              $this->db->from('modelos');
    //              $this->db->where('Id_Modelo', $id);
    //             $query = $this->db->get(); 
    //             return $query->result();
    // }
    // public function UpdateModelo($id)
    // {

    //     try{
    //         $this->db->set('Estado', 2,false);
    //         $this->db->where('Id_Modelo', $id);
    //         $query =  $this->db->update('modelos');
    //         return $query;
           
    //     } catch (Exception $e) {
    //         echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    //     }

    // }
}

