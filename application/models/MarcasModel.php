<?php
class MarcasModel extends CI_Model{
    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }

    function SaveMarca($id,$Descripcion,$estado)
    {
        $status = $estado == "on" ? 1 : 2;
        
        if ($id > 0) {
            $array = array(
                'Descripcion' => $Descripcion,
                'Estado' => $status
        );
            $this->db->set($array);
            $this->db->where('Id_Marca', $id);
            $query =  $this->db->update('marcas');
            return $query;
        }else{
            $data = array(
                'Descripcion' => $Descripcion,
                'Estado' => $status
                );
            $query =   $this->db->insert('marcas', $data);
            return $query;
        }

    }

    public function getAllMarcas($all)
    {
        if ($all == 1) {
            $this->db->select('Id_Marca,Descripcion, Estado');
            $this->db->from('marcas');
            $this->db->where('Estado', 1);
            $query = $this->db->get(); 
            return $query->result();
        }else{
            $this->db->select('Id_Marca,Descripcion, Estado');
            $this->db->from('marcas');
            $query = $this->db->get(); 
            return $query->result();
        }

    }

    public function getMarcaById($id)
    {
                 $this->db->select('Id_Marca,Descripcion, Estado');
                 $this->db->from('marcas');
                 $this->db->where('Id_Marca', $id);
                $query = $this->db->get(); 
                return $query->result();
    }
    public function UpdateMarca($id)
    {

        try{
            $this->db->set('Estado', 2,false);
            $this->db->where('Id_Marca', $id);
            $query =  $this->db->update('marcas');
            return $query;
           
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

    }
}

