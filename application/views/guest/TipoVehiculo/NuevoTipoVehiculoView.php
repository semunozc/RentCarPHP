<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h3>Nuevo tipo Vehiculo</h3>
        </div>
        <br>
        <!-- Widgets -->
        <div class="row clearfix">
            <div class="row clearfix js-sweetalert">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <form id="FormNewTipoVehiculo" >
                                <div class="form-group form-float">
                                    <div class="form-line"><input type="text" name="Descripcion" id="Descripcion" class="form-control" required>
                                        <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                        <label class="form-label">Descripcion</label>
                                    </div>
                                </div>
                                <input type="checkbox" name="Activo" id="remember_me_2" class="filled-in" > 
                                <label for="remember_me_2">Activo</label>
                                <br>
                                <button type="button" class="btn btn-primary waves-effect" id="btn_NewVehiculo" data-type="ajax-loader">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 