
<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar-collapse"
                    aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url();?>Home">Rent A Car</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search"
                            data-close="true"><i class="material-icons">search</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url();?>plantilla/images/user.png"
                    width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><?php echo  $NombreCompleto ?></div>
                        <div class="email"><?php echo $Correo?></div>
                            <div class="btn-group user-helper-dropdown"> <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                    <li role="seperator" class="divider"></li>
                                    <li><a href="<?php echo base_url();?>LoginController/Logout"> <i class="material-icons">input</i>Sign Out</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="menu">
                        <ul class="list">
                            <li class="header">MAIN NAVIGATION</li>
                            <?php if ($active == 'home') { ?>
                                <li class="active">
                             <?php }else{?>
                                <li>
                             <?php }?>
                                <a href="/RentCarPHP/home"> <i class="material-icons">dashboard</i><span>Dashboard</span></a>
                            </li>
                            <?php if ($active == 'Mantenimientos') { ?><li class="active"><?php }else{?><li><?php }?>                         
                                <a href="javascript:void(0);" class="menu-toggle"><i class="material-icons">assignment</i><span>Mantenimientos</span> </a>
                                <ul class="ml-menu">
                                    <li class="form-inline"><a href="javascript:void(0);"><i class="material-icons">group_add</i><span>Usuarios</span></a></li>
                                    <li>
                                        <a href="<?php echo base_url();?>Clientes"><i class="material-icons">people</i><span>Cliente</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>Marcas"><i class="material-icons">branding_watermark</i><span>Marcas</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>Modelos"><i class="material-icons">class</i><span>Modelos</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>TipoVehiculos"><i class="material-icons">local_car_wash</i><span>Tipo de Vehiculos</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>TipoCombustibles"><i class="material-icons">local_gas_station</i><span>Tipo de Combustible</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>Vehiculos"><i class="material-icons">drive_eta</i><span>Vehiculos</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                            <?php if ($active == 'Rent') { ?><li class="active"><?php }else{?><li><?php }?>  
                                <a href="javascript:void(0);" class="menu-toggle"><i class="material-icons">vpn_key</i>
                                    <span>Renta y Devolución de vehiculos</span>
                                </a>
                                <ul class="ml-menu">
                                
                                    <li>
                                        <a href="<?php echo base_url();?>Renta">Renta de Vehiculo</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>Devolucion">Devolución de Vehiculo</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="header">LABELS</li>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="material-icons col-red">donut_large</i>
                                    <span>Important</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="material-icons col-amber">donut_large</i>
                                    <span>Warning</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="material-icons col-light-blue">donut_large</i>
                                    <span>Information</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </aside>
            </section>