<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h3>Renta de Vehiculos</h3>
        </div>
        <br>
        <div class="row">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <!-- <div> <?php echo $form?> </div> -->
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);"
                                        class="dropdown-toggle"
                                        data-toggle="dropdown" role="button"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url();?>Vehiculos/AddVehiculo"><i class="material-icons">add_circle</i>Nuevo</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped
                                    table-hover dataTable js-exportable"
                                    id="mainTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Descripcion</th>
                                            <th>No. Chasis</th>
                                            <th>No. Motor</th>
                                            <th>No. Placa</th>
                                            <th>Tipo de Vehiculo</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Tipo de Combustible</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Descripcion</th>
                                            <th>No. Chasis</th>
                                            <th>No. Motor</th>
                                            <th>No. Placa</th>
                                            <th>Tipo de Vehiculo</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Tipo de Combustible</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($RentVehiculos as $key) {  ?>
                                        <tr>
                                            <td><?php echo $key->Id_Vehiculo?></td>
                                            <td><?php echo $key->Descripcion?></td>
                                            <td><?php echo $key->No_Chasis?></td>
                                            <td><?php echo $key->No_Motor?></td>
                                            <td><?php echo $key->No_Placa?></td>
                                            <td><?php echo $key->tipovehiculo?></td>
                                            <td><?php echo $key->marca?></td>
                                            <td><?php echo $key->modelo?></td>
                                            <td><?php echo $key->combustible?></td>
                                            <td><?php echo $key->estado == 1? 'Activo':'inactivo'  ?></td>
                                            <td>
                                                <button type="button" onclick="rentarVehiculo(<?php echo $key->Id_Vehiculo?>);" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#largeModal"><i class="material-icons">eject</i> Rentar</button>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </div>
            <!-- Large Size -->
                <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <input type='hidden' name="id_vehiculo" id="id_vehiculo" class="form-control" required>
                                <h4 class="modal-title" id="largeModalLabel">Seleccione Cliente</h4>
                            </div>
                            <div class="modal-body">
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped
                                        table-hover dataTable js-exportable"
                                        id="mainTable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Apellido</th>
                                                <th>Cedula</th>
                                                <th>No de Tarjeta</th>
                                                <th>Límite de Credito</th>
                                                <th>Tipo_Persona</th>
                                                <th>Estado</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Apellido</th>
                                                <th>Cedula</th>
                                                <th>No de Tarjeta</th>
                                                <th>Límite de Credito</th>
                                                <th>Tipo_Persona</th>
                                                <th>Estado</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php foreach ($AllClient as $key) {  ?>
                                            <tr>
                                                <td><?php echo $key->Id_Cliente?></td>
                                                <td><?php echo $key->Nombre?></td>
                                                <td><?php echo $key->Apellido?></td>
                                                <td><?php echo $key->Cedula?></td>
                                                <td><?php echo $key->No_Tarjeta?></td>
                                                <td><?php echo $key->Límite_Credito?></td>
                                                <td><?php echo $key->Tipo_Persona?></td>
                                                <td><?php echo $key->Estado == 1 ? 'Activo' : 'Inactivo'?></td>
                                                <td>
                                                    <button type="button" onclick="SelectCliente(<?php echo $key->Id_Cliente?>);" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#largeModal"><i class="material-icons">check_circle</i></button>
                
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                                <!-- <button type="button" class="btn btn-primary  waves-effect" id="btn_NewVehiculo"  data-type="ajax-loader">Guardar</button>
                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>-->
                        </div>
                    </div>
                </div>
                </div>
                            <!-- Large Size -->
              <div class="modal fade" id="largeModal2" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Rentar Vehiculo</h4>
                        </div>
                        <div class="modal-body">
                            <input type='hidden' name="id" id="id" class="form-control" >
                            <input type='hidden'  name="id_vehiculo1" id="id_vehiculo1" class="form-control" >
                            <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                            <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="">Fecha Devolución</label>
                                        <input type="date" name="Fecha_devolucion" id="Fecha_devolucion" class="form-control" required>
                                        <div id ="container" > </div>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="">Monto x Día</label>
                                        <input type="text" name="MontoXDia" id="MontoXDia" class="form-control" required>
                                        <div id ="container" > </div>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="">Cantidad de días</label>
                                        <input type="text" name="CantidadDias" id="CantidadDias" class="form-control" required>
                                        <div id ="container" > </div>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                    <label class="">Comentarios</label>
                                    <textarea name="comentarios" id ="Comentarios" rows="10" class="form-control" cols="40"></textarea>
                                    </div>
                                </div>
                            <div>
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success waves-effect" onclick="Inspeccionar();"><i class="material-icons">arrow_forward</i></button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="material-icons">close</i></button>
                        </div>
                    </div>
                </div>
              </div>

                <div class="modal fade" id="largeModal3" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Inspeccion</h4>
                        </div>

                        <form id ="RentarVheiculo">
                        <div class="modal-body">
                            <input  type='hidden' name="id2" id="id2" class="form-control" required>
                            <input  type='hidden' name="id_vehiculo2" id="id_vehiculo2" class="form-control" required>
                            <input  type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                            <input  type='hidden' name="Fecha_devolucion2" id="Fecha_devolucion2" class="form-control" required>
                            <input  type='hidden' name="MontoXDia2" id="MontoXDia2" class="form-control" required>
                            <input  type='hidden' name="CantidadDias2" id="CantidadDias2" class="form-control" required>
                            <input  type='hidden' name="Comentarios2" id="Comentarios2" class="form-control" required>

                            <div>
                              <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="body">
                                        <form>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene Ralladuras</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_ralladuras" name= "ralladura" value = '1'  class="filled-in">
                                                    <label for="si_ralladuras">Si</label>
                                                    <input type="checkbox" id="no_ralladuras" name= "ralladura"  value = '2'  class="filled-in">
                                                    <label for="no_ralladuras">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Cantidad Combustible</p>
                                                    <div  class="form-inline">
                                                    <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" value = '1/4' name ="combstible" id="ig_checkbox">
                                                    <label for="ig_checkbox">1/4</label>
                                                   </span>
                                                   <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" value = '1/2' name ="combstible" id="ig_checkbox2">
                                                    <label for="ig_checkbox2">1/2</label>
                                                   </span>
                                                   <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" value = '3/4' name ="combstible" id="ig_checkbox3">
                                                    <label for="ig_checkbox3">3/4</label>
                                                   </span>
                                                   <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" value = 'lleno' name ="combstible" id="ig_checkbox4">
                                                    <label for="ig_checkbox4">lleno</label>
                                                   </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene Goma respuesta</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_Goma" value = 'Si'  name ="Goma" class="filled-in">
                                                    <label for="si_Goma">Si</label>
                                                    <input type="checkbox" id="no_Goma" name ="Goma" value = 'No'  class="filled-in">
                                                    <label for="no_Goma">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene Gato</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_Gato" value = 'Si' name ="Gato" class="filled-in">
                                                    <label for="si_Gato">Si</label>
                                                    <input type="checkbox" id="no_Gato" value = 'No'  name ="Gato" class="filled-in">
                                                    <label for="no_Gato">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene roturas cristal</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_cristal" value = 'Si' name ="cristal" class="filled-in">
                                                    <label for="si_cristal">Si</label>
                                                    <input type="checkbox" id="no_cristal" value = 'No' name ="cristal" class="filled-in">
                                                    <label for="no_cristal">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                 <p>Estado gomas </p>
                                                <div class="form-line">
                                                   <p>gomas 1</p>
                                                    <div>
                                                    <input type="checkbox" name = "goma1" value = '5' id="Estado_gomasBuena1" class="filled-in">
                                                    <label for="Estado_gomasBuena1">Buena Condicion </label>
                                                    <input type="checkbox" name = "goma1" value = '6' id="Estado_gomasMala1" class="filled-in">
                                                    <label for="Estado_gomasMala1">Mala Condicion</label>
                                                    </div>
                                                    <p>gomas 2</p>
                                                    <div>
                                                    <input type="checkbox" name = "goma2" value = '5' id="Estado_gomasBuena2" class="filled-in">
                                                    <label for="Estado_gomasBuena2">Buena Condicion </label>
                                                    <input type="checkbox" name = "goma2" value = '6' id="Estado_gomasMala2" class="filled-in">
                                                    <label for="Estado_gomasMala2">Mala Condicion</label>
                                                    </div>
                                                    <p>gomas 3</p>
                                                    <div>
                                                    <input type="checkbox" name = "goma3" value = '5' id="Estado_gomasBuena3" class="filled-in">
                                                    <label for="Estado_gomasBuena3">Buena Condicion </label>
                                                    <input type="checkbox" name = "goma3" value = '5' id="Estado_gomasMala3" class="filled-in">
                                                    <label for="Estado_gomasMala3">Mala Condicion</label>
                                                    </div>
                                                    <p>gomas 4</p>
                                                    <div>
                                                    <input type="checkbox" name = "goma4" value = '5' id="Estado_gomasBuena4" class="filled-in">
                                                    <label for="Estado_gomasBuena4">Buena Condicion </label>
                                                    <input type="checkbox" name = "goma4" value = '6' id="Estado_gomasMala4" class="filled-in">
                                                    <label for="Estado_gomasMala4">Mala Condicion</label>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <br>
                                            <button type="button" class="btn btn-success m-t-15 waves-effect terminar"><i class="material-icons">check</i> Terminar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
</section>