<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h3>Nuevo Vehiculo</h3>
        </div>
        <br>
        <br>
        <!-- Widgets -->
        <div class="row clearfix">
            <div class="row clearfix js-sweetalert">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <form id="FormNewVehiculorent" action ='' method = "post">
                                <div class="form-group form-float">
                                    <div class="form-line"><input type="text" name="Descripcion" id="Descripcion" class="form-control" value ="<?php if (isset($_POST['Descripcion'])) { echo $_POST['Descripcion'];}?>" required>
                                        <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                        <label class="form-label">Descripcion</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line"><input type="text" name="No_Chasis" id="No_Chasis" class="form-control" value ="<?php if (isset($_POST['No_Chasis'])) { echo $_POST['No_Chasis'];}?>" required>
                                        <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                        <label class="form-label">No de Chasis</label>
                                    </div>
                                    </div>
                                  <div class="form-group form-float">
                                    <div class="form-line"><input type="text" name="No_Motor" id="No_Motor" class="form-control" value ="<?php if (isset($_POST['No_Motor'])) { echo $_POST['No_Motor'];}?>" required>
                                        <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                        <label class="form-label">No de Motor</label>
                                    </div>
                                    </div>
                                    <div class="form-group form-float">
                                    <div class="form-line"><input type="text" name="No_Placa" id="No_Placa" class="form-control" value ="<?php if (isset($_POST['No_Placa'])) { echo $_POST['No_Placa'];}?>" required>
                                        <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                        <label class="form-label">No de Placa</label>
                                    </div>
                                    </div>
                                    <div class="form-group form-float">
                                    <div>
                                        <select class="form-control show-tick" name="Tipo_Vehiculo" id="Tipo_Vehiculo" >
                                        <option value="0">Seleccione Tipo de Vehiculo</option>
                                        <?php foreach ($TipoVehiculo as $key) {?>
                                                <option value="<?php echo $key->Id?>"
                                                 <?php if (isset($_POST['Tipo_Vehiculo'])) { if( $_POST["Tipo_Vehiculo"] == $key->Id ) { echo "selected='selected'"; }}?>  >
                                                <?php echo $key->Descripcion?></option><?php }?>
                                        </select>
                                    </div>
                                    </div>
                                   <div class="form-group form-float">
                                    <div class="form-float">
                                        <select class="form-control show-tick" name="idMarca" id="idMarca"  required>
                                            <option value="0">Seleccione Marca</option>
                                            <?php foreach ($AllMarcas  as $key) {?>
<option value="<?php echo $key->Id_Marca?>"<?php if (isset($_POST['idMarca'])) { if( $_POST["idMarca"] == $key->Id_Marca ) { echo "selected='selected'"; }}?>><?php echo $key->Descripcion?></option><?php }?>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="form-group form-float">
                                    <div>
                                        <select class="form-control show-tick" name="idModelo" id="idModelo" >
                                        <option value="0">Seleccione Modelo</option>
<?php foreach ($modelo as $key) {?> <option value="<?php echo $key->Id_Modelo?>"><?php echo $key->Modelo?></option> <?php }?>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="form-group form-float">
                                    <div>
                                        <select class="form-control show-tick" name="IdCombustible" id="IdCombustible" >
                                        <option value="0">Seleccione Tipo de Combustible</option>
                                        <?php foreach ($TipoConbustible as $key) {?>
                                                <option value="<?php echo $key->Id_Combustible?>"
                                                 <?php if (isset($_POST['IdCombustible'])) { if( $_POST["IdCombustible"] == $key->Id_Combustible ) { echo "selected='selected'"; }}?>  >
                                                <?php echo $key->Descripcion?></option><?php }?>
                                        </select>
                                    </div>
                                    </div>
                                <input type="checkbox" name="Activo" id="remember_me_2" class="filled-in" > 
                                <label for="remember_me_2">Activo</label>
                                <br>
                                <button type="button" class="btn btn-primary waves-effect" id="btn_NewVehiculorent" data-type="ajax-loader">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 