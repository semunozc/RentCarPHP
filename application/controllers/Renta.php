<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Renta extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
		$this->load->model("RentModel");
		$this->load->model("ClientModel");
		$this->load->model("VehiculoModel");
		
	}
	public function index()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Rentar');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Rent');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin

			$data =  array('RentVehiculos' => $this->RentModel->getAllVehiculos(1),'AllClient' => $this->ClientModel->getAllClient(1));
			$this->load->view('guest/Renta/RentaVehiculoView',$data );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
	}
	public function SaveVehiculoRentado()
	{
		$id_empleado = $this->session->userdata('id');
		$id_Cliente =$this->input->post('id2');
		$id_vehiculo =$this->input->post('id_vehiculo2');
		$Fecha_devolucion =$this->input->post('Fecha_devolucion2');
		$MontoXDia2 =$this->input->post('MontoXDia2');
		$CantidadDias2 =$this->input->post('CantidadDias2');
		$Comentarios2 =$this->input->post('Comentarios2');
		$tipo = $this->input->post('tipo');
		$Activo =$this->input->post('Activo');
		$ralladura =$this->input->post('ralladura');
		$combstible =$this->input->post('combstible');
		$Gomarepuesto =$this->input->post('Goma');
		$Gato =$this->input->post('Gato');
		$cristal =$this->input->post('cristal');
		$goma1 =$this->input->post('goma1');
		$goma2 =$this->input->post('goma2');
		$goma3 =$this->input->post('goma3');
		$goma4 =$this->input->post('goma4');
		$array = ["goma1" => $goma1,"goma2" => $goma2,"goma3" => $goma3,"goma4" => $goma4];
		 $result = $this->RentModel->SaveInspeccionVehiculo($id_vehiculo,$id_Cliente,$ralladura,$combstible,$Gomarepuesto,$Gato,$cristal,$array,$id_empleado);

		 if ($result == 1) {
			$result = $this->RentModel->SaveVehiculoRentado($id_empleado,$id_vehiculo,$id_Cliente,$Fecha_devolucion,$MontoXDia2,$CantidadDias2,$Comentarios2);
			if ($result == 1) {
			$result2 = $this->VehiculoModel->UpdateVehiculo($id_vehiculo);
			echo $result2;
			}
		 }

	}
}
