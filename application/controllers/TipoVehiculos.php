<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipoVehiculos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
		
	}
	public function index()
	{
		$user_data = $this->session->userdata('login');
		$id =$this->input->post('all');
		$all = $id;
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Tipo de Vehiculo');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin
			if ($all == 1) {
				$this->load->model("TipoVehiculoModel");
				$data =  array('ConteoTipoVehivulos' => $this->TipoVehiculoModel->getAllTipoVehivulos($all),
						'form' => "<form id='target' method='post' action=''>
						<select class='show-tick' name='all' id ='all' required>
							<option value=''>Activos</option>
							<option value='2'>Todos</option>
						</select>
					</form>");
				$this->load->view('guest/TipoVehiculo/TipoVehiculoView',$data );
			}else{
				$this->load->model("TipoVehiculoModel");
				$data =  array('ConteoTipoVehivulos' => $this->TipoVehiculoModel->getAllTipoVehivulos($all),
						'form' => "<form id='target' method='post' action=''>
						<select class='show-tick' name='all' id ='all' required>
							<option value='2'>Todos</option>
							<option value='1'>Activos</option>
						</select>
					</form>");
				$this->load->view('guest/TipoVehiculo/TipoVehiculoView',$data );
			}



			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
	}
	public function AddTipoVehiculo()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Nuevo Vehiculo');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin
			$this->load->view('guest/TipoVehiculo/NuevoTipoVehiculoView' );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
	}
	public function SaveTipoVehiculo()
	{
		$id =$this->input->post('id');
		$Descripcion =$this->input->post('Descripcion');
		$Activo =$this->input->post('Activo');

		$this->load->model("TipoVehiculoModel");
		$result = $this->TipoVehiculoModel->SaveTipoVehiculo($id,$Descripcion,$Activo);
		echo $result;
	}
	public function UpdateTipoVehiculo()
	{
		$id = $this->input->post('id');
		$this->load->model("TipoVehiculoModel");
		$result = $this->TipoVehiculoModel->UpdateTipoVehiculo($id);
		echo $result;
	}
	public function SelectTipoVehiculos()
	{
		$id = $this->input->post('id');
		$this->load->model("TipoVehiculoModel");
		$result = $this->TipoVehiculoModel->getTipoVehiculosById($id);
		echo json_encode($result);
	}
}
