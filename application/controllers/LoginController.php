<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
			header('Access-Control-Allow-Origin: *'); 
	    	$this->load->model("UserModel");
			
	}
	public function index()
	{
			 $user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Home');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'home');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin

			$this->load->model("UserModel");
			$data =  array(
				'ConteoClientes' => $this->UserModel->getAll(),
			);
			$this->load->view('guest/Home',$data );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}

	}


	public function Login()
	{
		$email =$this->input->post('email');
		$password =$this->input->post('password');
		if (!empty($email) and !empty($password) ) {
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$user_data = $this->UserModel->getUserData($email,$password);
				if ($user_data != false) {
					$data = array 
					(
						'email' => $user_data[0]->Usuario,
						'id'=> $user_data[0]->Id_Usuario,
						'Nombre'=> $user_data[0]->Nombre." ".$user_data[0]->Apellido,
						'Role'=> $user_data[0]->Role,
						'login'=> true
					);
					$this->session->set_userdata($data);
					echo true;
				}else{
					echo "Usuario o contraseña invalido.";
				}


			}else{
				//echo "Esta dirección de correo ($email) no es válida.";
			}
		}else{
			echo "Debe completar los campos";
		}
	}
	public function Logout()
	{
		$this->session->sess_destroy();
		header("Location:" . base_url());
	}
}
