<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
		
	}
	public function index()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Home');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,$Correo,'active' => 'home');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin

			$this->load->model("UserModel");
			$data =  array('ConteoClientes' => $this->UserModel->getAll());
			$this->load->view('guest/Home',$data );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
	}
}
