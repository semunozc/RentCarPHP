<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Devolucion extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
		$this->load->model("RentModel");
		$this->load->model("ClientModel");
		$this->load->model("VehiculoModel");
		
	}
	public function index()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Devolucion');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Rent');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin

			$data =  array('RentVehiculos' => $this->RentModel->getAllVehiculos(3),'AllClient' => $this->ClientModel->getAllClient(1));
			$this->load->view('guest/Devolucion/DevolucionView',$data );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
	}
	public function SaveVehiculoRentado()
	{
		$id_empleado = $this->session->userdata('id');
		$id_Cliente =$this->input->post('id2');
		$id_vehiculo =$this->input->post('id_vehiculo1');
		$Fecha_devolucion =$this->input->post('Fecha_devolucion2');
		$MontoXDia2 =$this->input->post('MontoXDia2');
		$CantidadDias2 =$this->input->post('CantidadDias2');
		$Comentarios2 =$this->input->post('Comentarios2');
		$tipo = $this->input->post('tipo');
		$Activo =$this->input->post('Activo');
		$result2 = $this->VehiculoModel->UpdateVehiculo2($id_vehiculo);
		echo $result2;
	}
}
