<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marcas extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
		
	}
	public function index()
	{
        $user_data = $this->session->userdata('login');
        $id =$this->input->post('all');
		$all = $id;
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Marcas');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,$Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin

            if ($all == 1) {
                $this->load->model("MarcasModel");
                $data =  array('AllMarcas' => $this->MarcasModel->getAllMarcas($all),
                        'form' => "<form id='target' method='post' action=''>
                        <select class='show-tick' name='all' id ='all' required>
                            <option value=''>Activos</option>
                            <option value='2'>Todos</option>
                        </select>
                    </form>");
                $this->load->view('guest/Marca/MarcasView',$data );
            }else{
                $this->load->model("MarcasModel");
                $data =  array('AllMarcas' => $this->MarcasModel->getAllMarcas($all),
                        'form' => "<form id='target' method='post' action=''>
                        <select class='show-tick' name='all' id ='all' required>
                            <option value='2'>Todos</option>
                            <option value='1'>Activos</option>
                        </select>
                    </form>");
                $this->load->view('guest/Marca/MarcasView',$data );
            }

			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
        }
        
    }
    public function AddMarcas()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Nuevo Marcas');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin
			$this->load->view('guest/Marca/NuevoMarcasView' );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
    }
    public function SaveMarca()
	{
		$id =$this->input->post('id');
		$Descripcion =$this->input->post('Descripcion');
		$Activo =$this->input->post('Activo');

		$this->load->model("MarcasModel");
		$result = $this->MarcasModel->SaveMarca($id,$Descripcion,$Activo);
		echo $result;
	}
	public function UpdateMarca()
	{
		$id = $this->input->post('id');
		$this->load->model("MarcasModel");
		$result = $this->MarcasModel->UpdateMarca($id);
		echo $result;
	}
    public function SelectMarca()
	{
		$id = $this->input->post('id');
		$this->load->model("MarcasModel");
		$result = $this->MarcasModel->getMarcaById($id);
		echo json_encode($result);
	}
}
