﻿  //Clientes ===============================================================================
  $("#btn_login").click(function() {
    var url = $("#Url").val();
    $.post(
      url + "index.php/LoginController/Login",
      $("#FormLogin").serializeArray()
    ).then(function(data) {
      if (data == 1) {
        window.location.href = url + "Home";
      } else {
        $("#EmailValidation").html(data);
      }
    });
  });
  function editClient(id) {
    $("#id").val(id);
    $.post("index.php/Clientes/SelectClient", { id: id }).then(function(data) {
      var result = JSON.parse(data);
      console.log(result[0]);
      $("#nombre").val(result[0]["Nombre"]);
      $("#apellido").val(result[0]["Apellido"]);
      $("#Cedula").val(result[0]["Cedula"]);
      $("#credit").val(result[0]["No_Tarjeta"]);
      //$("#tipopersona").val(result[0]["Tipo_Persona"]);
      $("#Límite_Credito").val(result[0]["Límite_Credito"]);
      if (result[0]["Estado"] == 0) {
        $("#Activo").prop("checked", false);
      }else{
        $("#Activo").prop("checked", true);
      }
      $('#tipopersona').val(result[0]["Tipo_Persona"]).trigger("change");
    });
  }
  function deleteClient(id) {
    swal(
      {
        title: "Estas Seguro?",
        text: "No seras capas de recuperarlo",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        closeOnConfirm: false
      },
      function() {
        $.post("index.php/Clientes/UpdateClient", { id: id }).then(function(
          data
        ) {
          if (data == 1) {
            swal(
              {
                title: "Eliminado!",
                text: "Usuario eliminado correctamente.",
                type: "success"
              },
              function() {
                location.reload();
              }
            );
          } else {
            $("#EmailValidation").html(data);
          }
        });
      }
    );
  }
  $("#btn_NewClient").click(function() {
   var limite = $("#Límite_Credito").val();
   if (limite <= 0) {
    $("#container").append('<div style="color:red" class="goodbye">The value must be greater than zero</div>');
   }else{
    $("#container").hide();
   }
    if ($("#FormNewClient").valid() && limite > 0) {
      swal(
        {
          title: "Guardar",
          text: "Seguro que desea Guardar?",
          type: "info",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true
        },
        function() {
          var url = $("#Url").val();
          var ftb = $("#FormNewClient").serializeArray();
          $.post(url + "index.php/Clientes/SaveClinete", ftb).then(function(data ) {
            if (data == 1) {
              swal(
                {
                  title: "Guardado!",
                  text: "Usuario guardado correctamente.",
                  type: "success"
                },
                function() {
                  window.location.href = url + "Clientes";
                }
              );
            } else {
              $("#EmailValidation").html(data);
            }
          });
        }
      );
    }
    //Custom Validations ===============================================================================
    //Date
    $.validator.addMethod(
      "customdate",
      function(value, element) {
        return value.match(/^\d\d\d\d?-\d\d?-\d\d$/);
      },
      "Please enter a date in the format YYYY-MM-DD."
    );

    //Credit card
    $.validator.addMethod(
      "creditcard",
      function(value, element) {
        return value.match(/^\d\d\d\d?-\d\d\d\d?-\d\d\d\d?-\d\d\d\d$/);
      },
      "Please enter a credit card in the format XXXX-XXXX-XXXX-XXXX."
    );
    //==================================================================================================
  });

  $("#all").change(function(){
    $( "#target" ).submit();
});

 //Marcas ===============================================================================
$("#btn_NewMarca").click(function() {
  if ($("#FormNewMarca").valid()) {
    swal(
      {
        title: "Guardar",
        text: "Seguro que desea Guardar?",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function() {
        var url = $("#Url").val();
        var ftb = $("#FormNewMarca").serializeArray();
        $.post(url + "index.php/Marcas/SaveMarca", ftb).then(function(data ) {
          if (data == 1) {
            swal(
              {
                title: "Guardado!",
                text: "Marca guardada correctamente.",
                type: "success"
              },
              function() {
                window.location.href = url + "Marcas";
              }
            );
          } else {
            $("#EmailValidation").html(data);
          }
        });
      }
    );
  }
});

function editMarca(id) {
  $("#id").val(id);
  $.post("index.php/Marcas/SelectMarca", { id: id }).then(function(data) {
    var result = JSON.parse(data);
    console.log(result[0]);
    $("#Descripcion").val(result[0]["Descripcion"]);
    if (result[0]["Estado"] == 0) {
      $("#Activo").prop("checked", false);
    }else{
      $("#Activo").prop("checked", true);
    }
  });
}

function deleteMarca(id) {
  swal(
    {
      title: "Estas Seguro?",
      text: "No seras capas de recuperarlo",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si",
      closeOnConfirm: false
    },
    function() {
      $.post("index.php/Marcas/UpdateMarca", { id: id }).then(function(
        data
      ) {
        if (data == 1) {
          swal(
            {
              title: "Eliminado!",
              text: "Usuario eliminado correctamente.",
              type: "success"
            },
            function() {
              location.reload();
            }
          );
        } else {
          $("#EmailValidation").html(data);
        }
      });
    }
  );
}
 //Tipo de Vehiculo ===============================================================================
 $("#btn_NewVehiculo").click(function() {
  if ($("#FormNewTipoVehiculo").valid()) {
    swal(
      {
        title: "Guardar",
        text: "Seguro que desea Guardar?",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function() {
        var url = $("#Url").val();
        var ftb = $("#FormNewTipoVehiculo").serializeArray();
        $.post(url + "index.php/TipoVehiculos/SaveTipoVehiculo", ftb).then(function(data ) {
          if (data == 1) {
            swal(
              {
                title: "Guardado!",
                text: "Usuario guardado correctamente.",
                type: "success"
              },
              function() {
                window.location.href = url + "TipoVehiculos";
              }
            );
          } else {
            $("#EmailValidation").html(data);
          }
        });
      }
    );
  }
});
function editTipoVehiculo(id) {
  $("#id").val(id);
  $.post("index.php/TipoVehiculos/SelectTipoVehiculos", { id: id }).then(function(data) {
    var result = JSON.parse(data);
    $("#Descripcion").val(result[0]["Descripcion"]);
    if (result[0]["Estado"] == 0) {
      $("#Activo").prop("checked", false);
    }else{
      $("#Activo").prop("checked", true);
    }
    $('#tipopersona').val(result[0]["Tipo_Persona"]).trigger("change");
  });
}
function deleteTipoVehiculo(id) {
  swal(
    {
      title: "Estas Seguro?",
      text: "No seras capas de recuperarlo",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si",
      closeOnConfirm: false
    },
    function() {
      $.post("index.php/TipoVehiculos/UpdateTipoVehiculo", { id: id }).then(function(
        data
      ) {
        if (data == 1) {
          swal(
            {
              title: "Eliminado!",
              text: "Usuario eliminado correctamente.",
              type: "success"
            },
            function() {
              location.reload();
            }
          );
        } else {
          $("#EmailValidation").html(data);
        }
      });
    }
  );
}
 //Tipo de Combustible ===============================================================================
 $("#btn_Newcombustible").click(function() {
  if ($("#FormNewTipocombustible").valid()) {
    swal(
      {
        title: "Guardar!",
        text: "Seguro que decea guardarlo.",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function() {
        var url = $("#Url").val();
        var ftb = $("#FormNewTipocombustible").serializeArray();
        $.post(url + "index.php/TipoCombustibles/SaveTipoConbustible", ftb).then(function(data ) {
          if (data == 1) {
            swal(
              {
                title: "Guardado!",
                text: "Usuario guardado correctamente.",
                type: "success"
              },
              function() {
                window.location.href = url + "TipoCombustibles";
              }
            );
          } else {
            $("#EmailValidation").html(data);
          }
        });
      }
    );
  }
});

function editTipocombustible(id) {
  $("#id").val(id);
  $.post("index.php/TipoCombustibles/SelectTipoConbustibles", { id: id }).then(function(data) {
      var result = JSON.parse(data);
      console.log(result[0]);
      $("#Descripcion").val(result[0]["Descripcion"]);
      if (result[0]["Estado"] == 0) {
        $("#Activo").prop("checked", false);
      }else{
        $("#Activo").prop("checked", true);
      }
    });
}
function deleteTipocombustible(id) {
  swal(
    {
      title: "Estas Seguro?",
      text: "No seras capas de recuperarlo",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si",
      closeOnConfirm: false
    },
    function() {
      $.post("index.php/TipoCombustibles/UpdateTipoConbustible", { id: id }).then(function(
        data
      ) {
        if (data == 1) {
          swal(
            {
              title: "Eliminado!",
              text: "Combustible eliminado correctamente.",
              type: "success"
            },
            function() {
              location.reload();
            }
          );
        } else {
          $("#EmailValidation").html(data);
        }
      });
    }
  );
}

 //Modelos ===============================================================================
 $("#btn_NewModelo").click(function() {
  if ($("#FormNewModelo").valid()) {
    swal(
      {
        title: "Guardar!",
        text: "Seguro que decea guardarlo.",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function() {
        var url = $("#Url").val();
        var ftb = $("#FormNewModelo").serializeArray();
        $.post(url + "index.php/Modelos/SaveModelo", ftb).then(function(data ) {
          if (data == 1) {
            swal(
              {
                title: "Guardado!",
                text: "Usuario guardado correctamente.",
                type: "success"
              },
              function() {
                window.location.href = url + "Modelos";
              }
            );
          } else {
            $("#EmailValidation").html(data);
          }
        });
      }
    );
  }
});
function editModelo(id) {
  $("#id").val(id);
  $.post("index.php/Modelos/SelectModelo", { id: id }).then(function(data) {
    var result = JSON.parse(data);
    $("#Descripcion").val(result[0]["Descripcion"]);
    $('#Modelo').val(result[0]["Id_Marca"]).trigger("change");
    if (result[0]["Estado"] == 0) {
      $("#Activo").prop("checked", false);
    }else{
      $("#Activo").prop("checked", true);
    }
  });
}
function deleteModelo(id) {
  swal(
    {
      title: "Estas Seguro?",
      text: "No seras capas de recuperarlo",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si",
      closeOnConfirm: false
    },
    function() {
      $.post("index.php/Modelos/UpdateModelo", { id: id }).then(function(
        data
      ) {
        if (data == 1) {
          swal(
            {
              title: "Eliminado!",
              text: "Usuario eliminado correctamente.",
              type: "success"
            },
            function() {
              location.reload();
            }
          );
        } else {
          $("#EmailValidation").html(data);
        }
      });
    }
  );
}
 //Vehiculos ===============================================================================
 $("#idMarca").change(function(){
  $("#FormNewVehiculorent" ).submit();
});

$("#btn_NewVehiculorent").click(function() {
  if ($("#FormNewVehiculorent").valid()) {
    swal(
      {
        title: "Guardar",
        text: "Seguro que desea Guardar?",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function() {
        var url = $("#Url").val();
        var ftb = $("#FormNewVehiculorent").serializeArray();
        $.post(url + "index.php/Vehiculos/SaveVehiculoRent", ftb).then(function(data ) {
          if (data == 1) {
            swal(
              {
                title: "Guardado!",
                text: "Usuario guardado correctamente.",
                type: "success"
              },
              function() {
                window.location.href = url + "Vehiculos";
              }
            );
          } else {
            $("#EmailValidation").html(data);
          }
        });
      }
    )
  }
});
  
//Rentar vehiculo =============================================================================


function SelectCliente(id) {
  $("#id").val(id);
 var  id_vehiculo=  $("#id_vehiculo").val();
 var vehiculo = $("#id_vehiculo1").val(id_vehiculo);
  $('#largeModal2').modal('show') 
}
function rentarVehiculo(id) {
  $("#id_vehiculo").val(id);
}

function Inspeccionar() {
  var  id_Cliente=  $("#id").val();
  $("#id2").val(id_Cliente);
  var Id_vehiculo = $("#id_vehiculo1").val();
  $("#id_vehiculo2").val(Id_vehiculo);
  var Fecha_devolucion = $("#Fecha_devolucion").val();
  $("#Fecha_devolucion2").val(Fecha_devolucion);
  var MontoXDia = $("#MontoXDia").val();
  $("#MontoXDia2").val(MontoXDia);
  var CantidadDias = $("#CantidadDias").val();
  $("#CantidadDias2").val(CantidadDias);
  var Comentarios = $("#Comentarios").val();
  $("#Comentarios2").val(Comentarios);
 $('#largeModal2').modal('hide') 
  var  id_Cliente=  $("#id").val("");
 var Id_vehiculo = $("#id_vehiculo1").val("");
 var Fecha_devolucion = $("#Fecha_devolucion").val('');
 var MontoXDia = $("#MontoXDia").val("");
 var CantidadDias = $("#CantidadDias").val("");
 var Comentarios = $("#Comentarios").val("");
 $('#largeModal3').modal('show') 

}
  $(".terminar").click(function() {
  var url = $("#Url").val();
  swal(
    {
      title: "Guardar",
      text: "Seguro que desea Guardar?",
      type: "info",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    },
    function() {

      $.post(url + "index.php/Renta/SaveVehiculoRentado",$("#RentarVheiculo").serializeArray()).then(function(data) {
        if (data == 1) {
          swal(
            {
              title: "Guardado!",
              text: "Vehiculo Reantado guardado correctamente.",
              type: "success"
            },
            function() {
              window.location.href = url + "Renta";
            }
          );
        } else {
          $("#EmailValidation").html(data);
        }
        console.log(JSON.stringify(data))
      });
 
    }
  );

});
$(".terminar2").click(function() {
  var url = $("#Url").val();
  swal(
    {
      title: "Guardar",
      text: "Seguro que desea Guardar?",
      type: "info",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    },
    function() {

      $.post(url + "index.php/Devolucion/SaveVehiculoRentado",$("#RentarVheiculo").serializeArray()).then(function(data) {
        if (data == 1) {
          swal(
            {
              title: "Guardado!",
              text: "Vehiculo Reantado guardado correctamente.",
              type: "success"
            },
            function() {
              window.location.href = url + "Renta";
            }
          );
        } else {
          $("#EmailValidation").html(data);
        }
        console.log(JSON.stringify(data))
      });
 
    }
  );

});

function SelectClienteDevolucion(id) {
  $("#id2").val(id);
 var  id_vehiculo=  $("#id_vehiculo").val();
 var vehiculo = $("#id_vehiculo1").val(id_vehiculo);
 var id_Cliente = $("#id2").val(id);
  $('#largeModal3').modal('show') 
}